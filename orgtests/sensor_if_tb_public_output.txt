Chronologic VCS simulator copyright 1991-2011
Contains Synopsys proprietary information.
Compiler version E-2011.03-SP1_Full64; Runtime version E-2011.03-SP1_Full64;  Jan  7 13:42 2013
************ STARTING SIMULATION ************
light curtain activated after         256 clock cycles
velocity sensor still ok after         256 cycles
emergency brake activated, after         256 clock cycles
weight sensor still ok after         256 cycles
too much weight detected, after         256 clock cycles
smoke detected after         256 clock cycles
************ SIMULATION COMPLETE ************
$finish called from file "../tgdi_000/testbenches/sensor_if_tb_public.v", line 195.
$finish at simulation time             79425000
           V C S   S i m u l a t i o n   R e p o r t 
Time: 79425000 ps
CPU Time:      0.280 seconds;       Data structure size:   0.0Mb
Mon Jan  7 13:42:34 2013
