`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   23:22:37 11/25/2012
// Design Name:   floor_request_if
// Module Name:   C:/ise_counter/zeinab_janus_tgdi/test_floor_request_if.v
// Project Name:  zeinab_janus_tgdi
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: floor_request_if
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_floor_request_if;

	// Inputs
	reg CLK;
	reg RESET;
	reg [2:0] CURRENT_FLOOR;
	reg HALTED;
	reg [9:0] FLOOR_REQUEST_IN;

	// Outputs
	wire [9:0] ENLIGHT_BUTTONS;
	wire [9:0] FLOOR_REQUEST_OUT;

	// Instantiate the Unit Under Test (UUT)
	floor_request_if uut (
		.CLK(CLK), 
		.RESET(RESET), 
		.CURRENT_FLOOR(CURRENT_FLOOR), 
		.HALTED(HALTED), 
		.ENLIGHT_BUTTONS(ENLIGHT_BUTTONS), 
		.FLOOR_REQUEST_IN(FLOOR_REQUEST_IN), 
		.FLOOR_REQUEST_OUT(FLOOR_REQUEST_OUT)
	);

	initial begin
		// Initialize Inputs
		CLK = 0;
		RESET = 1;
		CURRENT_FLOOR = 0;
		HALTED = 0;
		FLOOR_REQUEST_IN = 0;

		// Wait 100 ns for global reset to finish
		#100;
		RESET = 0;
		#100;
		
		FLOOR_REQUEST_IN = 10'b0000100000;
		#100;
		FLOOR_REQUEST_IN = 10'b0000000000;
		#100;
		CURRENT_FLOOR = 3'b010;
		HALTED = 1;
		#100;
		RESET = 1;
		
		
        
		// Add stimulus here

	end
      
endmodule

