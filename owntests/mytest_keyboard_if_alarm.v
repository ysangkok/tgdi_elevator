`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   16:30:14 11/24/2012
// Design Name:   keyboard_if
// Module Name:   C:/ise_counter/zeinab_janus_tgdi/test_keyboard_if_alarm.v
// Project Name:  zeinab_janus_tgdi
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: keyboard_if
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_keyboard_if_alarm;

	// Inputs
	reg CLK;
	reg RESET;
	reg [4:0] FLOOR_SELECT;
	reg CLOSE_DOOR_IN;
	reg PASSENGER_ALARM_IN;
	reg [2:0] CLEAR_FLOOR_BUTTON;
	reg CLEAR_FLOOR_BUTTON_VALID;

	// Outputs
	wire CLOSE_DOOR_OUT;
	wire PASSENGER_ALARM_OUT;
	wire [4:0] SELECTED_FLOORS;
	wire [4:0] ENLIGHT_BUTTONS;

	// Instantiate the Unit Under Test (UUT)
	keyboard_if uut (
		.CLK(CLK), 
		.RESET(RESET), 
		.FLOOR_SELECT(FLOOR_SELECT), 
		.CLOSE_DOOR_IN(CLOSE_DOOR_IN), 
		.PASSENGER_ALARM_IN(PASSENGER_ALARM_IN), 
		.CLEAR_FLOOR_BUTTON(CLEAR_FLOOR_BUTTON), 
		.CLEAR_FLOOR_BUTTON_VALID(CLEAR_FLOOR_BUTTON_VALID), 
		.CLOSE_DOOR_OUT(CLOSE_DOOR_OUT), 
		.PASSENGER_ALARM_OUT(PASSENGER_ALARM_OUT), 
		.SELECTED_FLOORS(SELECTED_FLOORS), 
		.ENLIGHT_BUTTONS(ENLIGHT_BUTTONS)
	);

	initial begin
		// Initialize Inputs
		CLK = 0;
		RESET = 0;
		FLOOR_SELECT = 0;
		CLOSE_DOOR_IN = 0;
		PASSENGER_ALARM_IN = 0;
		CLEAR_FLOOR_BUTTON = 0;
		CLEAR_FLOOR_BUTTON_VALID = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		if (PASSENGER_ALARM_OUT !== 0) $display("failed test");
		PASSENGER_ALARM_IN = 1;
		#20;
		if (PASSENGER_ALARM_OUT !== 1) $display("failed test");
		RESET = 1;
		PASSENGER_ALARM_IN = 0;
		#20;
		if (PASSENGER_ALARM_OUT !== 0) $display("failed test");
	end
	
	always 
		#10 CLK = ~CLK;
      
endmodule

