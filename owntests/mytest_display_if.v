`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   21:28:48 11/25/2012
// Design Name:   display_if
// Module Name:   C:/ise_counter/zeinab_janus_tgdi/test_display_if.v
// Project Name:  zeinab_janus_tgdi
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: display_if
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_display_if;

	task setfloor;
		input[2:0] param1;
		begin;
		CURRENT_FLOOR[2:0] = param1;
		#10;
		end;
	endtask;

	// Inputs
	reg [2:0] CURRENT_FLOOR;

	// Outputs
	wire [6:0] ENABLE_SEGMENT;

	// Instantiate the Unit Under Test (UUT)
	display_if uut (
		.CURRENT_FLOOR(CURRENT_FLOOR), 
		.ENABLE_SEGMENT(ENABLE_SEGMENT)
	);

	initial begin
		// Initialize Inputs
		CURRENT_FLOOR = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		
			setfloor(3'b000);
			setfloor(3'b001);
			setfloor(3'b010);
			setfloor(3'b011);
			setfloor(3'b100);
			setfloor(3'b101);
			setfloor(3'b110);
			setfloor(3'b111);
		

	end
      
endmodule

