`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   00:46:24 11/26/2012
// Design Name:   sensor_if
// Module Name:   C:/ise_counter/zeinab_janus_tgdi/test_sensor_if.v
// Project Name:  zeinab_janus_tgdi
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: sensor_if
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_sensor_if;

	// Inputs
	reg CLK;
	reg RESET;
	reg DOOR_LIGHT_CURTAIN_IN;
	reg [10:0] VELOCITY_SENSOR_IN;
	reg [9:0] WEIGHT_SENSOR_IN;
	reg SMOKE_PARTICLE_SENSOR_IN;

	// Outputs
	wire DOOR_LIGHT_CURTAIN_OUT;
	wire VELOCITY_SENSOR_OUT;
	wire WEIGHT_SENSOR_OUT;
	wire SMOKE_PARTICLE_SENSOR_OUT;

	// Instantiate the Unit Under Test (UUT)
	sensor_if uut (
		.CLK(CLK), 
		.RESET(RESET), 
		.DOOR_LIGHT_CURTAIN_IN(DOOR_LIGHT_CURTAIN_IN), 
		.VELOCITY_SENSOR_IN(VELOCITY_SENSOR_IN), 
		.WEIGHT_SENSOR_IN(WEIGHT_SENSOR_IN), 
		.SMOKE_PARTICLE_SENSOR_IN(SMOKE_PARTICLE_SENSOR_IN), 
		.DOOR_LIGHT_CURTAIN_OUT(DOOR_LIGHT_CURTAIN_OUT), 
		.VELOCITY_SENSOR_OUT(VELOCITY_SENSOR_OUT), 
		.WEIGHT_SENSOR_OUT(WEIGHT_SENSOR_OUT), 
		.SMOKE_PARTICLE_SENSOR_OUT(SMOKE_PARTICLE_SENSOR_OUT)
	);

	initial begin
		// Initialize Inputs
		CLK = 0;
		RESET = 0;
		DOOR_LIGHT_CURTAIN_IN = 0;
		VELOCITY_SENSOR_IN = 0;
		WEIGHT_SENSOR_IN = 0;
		SMOKE_PARTICLE_SENSOR_IN = 0;

		// Wait 100 ns for global reset to finish
		#10;
		RESET=1;
		#5;
		RESET=0;
		#5;

		
        
		// Add stimulus here
		DOOR_LIGHT_CURTAIN_IN = 1;
		#50;
		WEIGHT_SENSOR_IN = 1000;
		#200;
		WEIGHT_SENSOR_IN = 900;
		#10;
		DOOR_LIGHT_CURTAIN_IN = 0;
		WEIGHT_SENSOR_IN = 0;
		#10;
		DOOR_LIGHT_CURTAIN_IN= 1;
		WEIGHT_SENSOR_IN = 920;
		#10;
		WEIGHT_SENSOR_IN = 930;
		#200;
		VELOCITY_SENSOR_IN = 1;
		DOOR_LIGHT_CURTAIN_IN = 0;
		#5;
		VELOCITY_SENSOR_IN = 0;
		#50;
		
		DOOR_LIGHT_CURTAIN_IN = 1;
		#50;
		//WEIGHT_SENSOR_IN = 1000;
		#450;
		VELOCITY_SENSOR_IN = 1;
		//DOOR_LIGHT_CURTAIN_IN = 0;
		#5;
		VELOCITY_SENSOR_IN = 0;
		#50;
		RESET = 1;
	end
	
	always 
		#1 CLK = ~CLK;
      
endmodule

