`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   16:51:44 11/24/2012
// Design Name:   keyboard_if
// Module Name:   C:/ise_counter/zeinab_janus_tgdi/test_keyboard_if.v
// Project Name:  zeinab_janus_tgdi
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: keyboard_if
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_keyboard_if;

integer TAP_f;
integer TAP_testnum = 1;
integer TAP_ntests;
parameter TAP_maxlen = 100;
reg [TAP_maxlen*8:0] TAP_tmpstr;

task TAP_start;
begin;
	$fwrite(TAP_f, "%d", TAP_testnum);
	$fwrite(TAP_f, "..");
	$fwrite(TAP_f, "%d", TAP_ntests );
	$fwrite(TAP_f, "\n");
end;
endtask;

task TAP_assert;
input val;
begin
	if (!val)
		$fwrite(TAP_f, "not ");
	$fwrite(TAP_f, "ok ");
	$fwrite(TAP_f, "%d", TAP_testnum);
	TAP_testnum = TAP_testnum + 1;
	$fwrite(TAP_f," - ");
	$fwrite(TAP_f,"%s",TAP_tmpstr);
	$fwrite(TAP_f,"\n");
end
endtask;

task TAP_end;
	$fclose(TAP_f);
endtask;

	// Inputs
	reg CLK;
	reg RESET;
	reg [4:0] FLOOR_SELECT;
	reg CLOSE_DOOR_IN;
	reg PASSENGER_ALARM_IN;
	reg [2:0] CLEAR_FLOOR_BUTTON;
	reg CLEAR_FLOOR_BUTTON_VALID;

	// Outputs
	wire CLOSE_DOOR_OUT;
	wire PASSENGER_ALARM_OUT;
	wire [4:0] SELECTED_FLOORS;
	wire [4:0] ENLIGHT_BUTTONS;

	// Instantiate the Unit Under Test (UUT)
	keyboard_if uut (
		.CLK(CLK), 
		.RESET(RESET), 
		.FLOOR_SELECT(FLOOR_SELECT), 
		.CLOSE_DOOR_IN(CLOSE_DOOR_IN), 
		.PASSENGER_ALARM_IN(PASSENGER_ALARM_IN), 
		.CLEAR_FLOOR_BUTTON(CLEAR_FLOOR_BUTTON), 
		.CLEAR_FLOOR_BUTTON_VALID(CLEAR_FLOOR_BUTTON_VALID), 
		.CLOSE_DOOR_OUT(CLOSE_DOOR_OUT), 
		.PASSENGER_ALARM_OUT(PASSENGER_ALARM_OUT), 
		.SELECTED_FLOORS(SELECTED_FLOORS), 
		.ENLIGHT_BUTTONS(ENLIGHT_BUTTONS)
	);

	initial begin
		TAP_f = $fopen("c:\\test_keyboard_if.tap");
	
		// Initialize Inputs
		CLK = 0;
		RESET = 1;
		FLOOR_SELECT = 0;
		CLOSE_DOOR_IN = 0;
		PASSENGER_ALARM_IN = 0;
		CLEAR_FLOOR_BUTTON = 0;
		CLEAR_FLOOR_BUTTON_VALID = 1;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		
		TAP_ntests = 5;
		TAP_start();
		
		RESET=0;
		#5;
		
		
		FLOOR_SELECT = 5'b00100;
		#5
		FLOOR_SELECT = 5'b01000;
		#5
		FLOOR_SELECT = 5'b0;
		#5;
		TAP_tmpstr = "enlight buttons test 1";
		TAP_assert(ENLIGHT_BUTTONS === 5'b01100);
		
		#100;
		
		CLEAR_FLOOR_BUTTON = 2;
		#5
		TAP_tmpstr = "enlight buttons test 2";
		TAP_assert(ENLIGHT_BUTTONS === 5'b01000);
		
		#100;
		
		TAP_tmpstr = "passenger alarm out 1";
		TAP_assert(PASSENGER_ALARM_OUT === 0);
		PASSENGER_ALARM_IN = 1;
		#5;
		TAP_tmpstr = "passenger alarm out 2";
		TAP_assert(PASSENGER_ALARM_OUT === 1);
		RESET = 1;
		PASSENGER_ALARM_IN = 0;
		#5;
		TAP_tmpstr = "passenger alarm out 3";
		TAP_assert(PASSENGER_ALARM_OUT === 0);
		
		//TAP_assert(0);
		
		#5;
		
		RESET = 0;
		TAP_end();
	end
	
	always 
		#1 CLK = ~CLK;
      
endmodule

