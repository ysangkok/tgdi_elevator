`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   01:27:15 12/05/2012
// Design Name:   sqrt
// Module Name:   C:/ise_counter/zeinab_janus_tgdi/test_sqrt.v
// Project Name:  zeinab_janus_tgdi
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: sqrt
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_sqrt;

	// Inputs
	reg CLK;
	reg RESET;
	reg NEXT;
	reg PREVIOUS;

	// Outputs
	wire [31:0] SQRT;

	// Instantiate the Unit Under Test (UUT)
	sqrt uut (
		.CLK(CLK), 
		.RESET(RESET), 
		.NEXT(NEXT), 
		.PREVIOUS(PREVIOUS), 
		.SQRT(SQRT)
	);

	initial begin
		// Initialize Inputs
		CLK = 0;
		RESET = 1;
		NEXT = 0;
		PREVIOUS = 0;

		// Wait 100 ns for global reset to finish
		#100;
		RESET = 0;
		#10;
        
		// Add stimulus here

	end
	always
		#10 CLK = ~ CLK;
      
endmodule

