// ----------------
// Project:
// ESA Elevator
// ----------------
//
// ----------------
// Group: 0
// 1234567: Wigald Boning
// 1234567: Oliver Dittrich
// ----------------
//
// Description:
// ----------------
// elevator floor request interface
//
// Version History:
// ----------------
// 121116: BT: TGDI WS12/13 v1.0

`timescale 1ns / 1ns

module floor_request_if

          #(parameter FLOORS                   =  5,
                      FLOOR_BITS               =  3)


          (input  wire                         CLK,
           input  wire                         RESET,

           input  wire  [(FLOOR_BITS-1):0]     CURRENT_FLOOR,
           input  wire                         HALTED,

           output wire  [((FLOORS*2)-1):0]     ENLIGHT_BUTTONS,

           input  wire  [((FLOORS*2)-1):0]     FLOOR_REQUEST_IN,     // connected to up/down buttons on the floors
           output wire  [((FLOORS*2)-1):0]     FLOOR_REQUEST_OUT);   //

/**************************** put your code here  ****************************/

endmodule
