// ----------------
// Project:
// ESA Elevator
// ----------------
//
// ----------------
// Group: 0
// 1234567: Wigald Boning
// 1234567: Oliver Dittrich
// ----------------
//
// Description:
// ----------------
// iterative sqrt computation with fixed delta
//
// Version History:
// ----------------
// 121116: BT: TGDI WS12/13 v1.0

`timescale 1ns / 1ns

module sqrt

          (input wire                      CLK,
           input wire                      RESET,
           input wire                      NEXT,
           input wire                      PREVIOUS,
           output reg  [31:0]              SQRT); //since we use 16.16 fix point, we need [31:0]
			  
  
  localparam  MAX_ERROR  =  32768;   // max_error = 0.5

/**************************** put your code here  ****************************/

endmodule









