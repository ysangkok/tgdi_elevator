// ----------------
// Project:
// ESA Elevator
// ----------------
//
// ----------------
// Group: 0
// 1234567: Wigald Boning
// 1234567: Oliver Dittrich
// ----------------
//
// Description:
// ----------------
// elevator cabin_ctrl
//
// Version History:
// ----------------
// 121116: BT: TGDI WS12/13 v1.0

`timescale 1ns / 1ns

module cabin_ctrl

          #(parameter FLOOR_BITS             =        3, // bits required for the defined number of floors
                      CONTROL_STEP_DISTANCE  =       10, // [mm]
                      DISTANCE_BITS_BUILDING =       14, // 12.000 mm
                      DISTANCE_BITS_FLOORS   =       12, // 12 bits <> 3000mm, required for height counter
                      FLOOR_HEIGHT           =     3000, // [mm]
                      WAIT_CYCLE             = 40000000, // wait 40 Mio cycles for a delay of 2 seconds @ 20 MHz
                      WAIT_CYCLE_BITS        =       26) // requires 26 bits


          (input  wire                                 CLK,
           input  wire                                 RESET,

           input  wire                                 MANUAL_DOOR_CLOSE,     // manual door close
           input  wire                                 MANUAL_ALARM,          // passenger alarm
           input  wire                                 OBJECT_DETECTED,       // object detected
           input  wire                                 SPEED_OK    ,          // accel_ok
           input  wire                                 WEIGHT_OK,             // weight_ok
           input  wire                                 SMOKE_DETECTED,        // smoke alarm
           input  wire  [(FLOOR_BITS-1)            :0] NEXT_FLOOR,            // next floor to visit
           input  wire                                 DOOR_MOTOR_DONE,       // door movement done
           input  wire                                 ELEVATOR_MOTOR_DONE,   // cabin movement done
           input  wire                                 ELEVATOR_MOTOR_TICK,   // elevator motor drove CONTROL_STEP_DISTANCE

           output reg   [(FLOOR_BITS-1)            :0] CURRENT_FLOOR,         // cabin is at this floor
           output reg                                  HALTED,                // halted at floor
           output wire                                 OPEN_DOOR,             // open cmd
           output wire                                 CLOSE_DOOR,            // close cmd
           output wire                                 ELEVATOR_UP,           // up cmd
           output wire                                 ELEVATOR_DOWN,         // down cmd
           output reg   [(DISTANCE_BITS_BUILDING-1):0] DISTANCE,              // go distance in milimeter
           output reg                                  EMERGENCY_BRAKE);      // emergency break cmd

/**************************** put your code here  ****************************/

endmodule
