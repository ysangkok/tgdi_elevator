// ----------------
// Project:
// ESA Elevator
// ----------------
//
// ----------------
// Group: 0
// 1234567: Wigald Boning
// 1234567: Oliver Dittrich
// ----------------
//
// Description:
// ----------------
// elevator keyboard interface
//
// Version History:
// ----------------
// 121116: BT: TGDI WS12/13 v1.0

`timescale 1ns / 1ns

module keyboard_if

          #(parameter FLOORS                    = 5,
                      FLOOR_BITS                = 3)


          (input  wire                           CLK,
           input  wire                           RESET,

           input  wire  [(FLOORS-1)          :0] FLOOR_SELECT,             // connected to floor select buttons
           input  wire                           CLOSE_DOOR_IN,            // connected to passenger close door button
           input  wire                           PASSENGER_ALARM_IN,       // connected to passenger alarm button
           input  wire  [(FLOOR_BITS-1)      :0] CLEAR_FLOOR_BUTTON,       // turn of the light for this encoded floor
           input  wire                           CLEAR_FLOOR_BUTTON_VALID, // if this signal is valid

           output wire                           CLOSE_DOOR_OUT,           // skip door close delay
           output wire                           PASSENGER_ALARM_OUT,      // passenger alarm
           output wire  [(FLOORS-1)          :0] SELECTED_FLOORS,          // cabin floor selection
           output wire  [(FLOORS-1)          :0] ENLIGHT_BUTTONS);         // turn button light on

/**************************** put your code here  ****************************/

endmodule

