// ----------------
// Project:
// ESA Elevator
// ----------------
//
// ----------------
// Group: 0
// 1234567: Wigald Boning
// 1234567: Oliver Dittrich
// ----------------
//
// Description:
// ----------------
// elevator next_floor_ctrl
//
// Version History:
// ----------------
// 121116: BT: TGDI WS12/13 v1.0

`timescale 1ns / 1ns

module next_floor_ctrl

          #(parameter FLOORS                    = 5,
                      FLOOR_BITS                = 3)


          (input  wire                           CLK,
           input  wire                           RESET,

           input  wire                           HALTED,           // cabin halted
           input  wire  [(FLOOR_BITS-1)      :0] CURRENT_FLOOR,    // curent floor (from cabin_ctrl)

           input  wire  [(FLOORS-1)          :0] DESTINATIONS,     // destinations selected (from keyboard_if)
           input  wire  [((FLOORS*2)-1)      :0] FLOOR_REQUEST,    // elevator requests (from floor_request_if)

           output reg   [(FLOOR_BITS-1)      :0] NEXT_FLOOR);      // next floor to be visisted

/**************************** put your code here  ****************************/

endmodule

