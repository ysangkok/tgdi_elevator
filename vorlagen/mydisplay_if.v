// ----------------
// Project:
// ESA Elevator
// ----------------
//
// ----------------
// Group: 0
// 1234567: Wigald Boning
// 1234567: Oliver Dittrich
// ----------------
//
// Description:
// ----------------
// elevator display interface
//
// Version History:
// ----------------
// 121116: BT: initial version

`timescale 1ns / 1ns

module display_if

          #(parameter FLOOR_BITS                =  3,
                      DISPLAY_SEGMENTS          =  7)


          (input  wire  [(FLOOR_BITS-1)      :0] CURRENT_FLOOR,    // encoded current floor input
           output wire  [(DISPLAY_SEGMENTS-1):0] ENABLE_SEGMENT);  // enable segments to show current floor

 /**************************** put your code here  ****************************/

endmodule
