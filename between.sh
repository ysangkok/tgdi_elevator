#!/bin/bash
start=`grep -n $1 < $3 | head -n1 | cut -d: -f1; exit ${PIPESTATUS[0]}`
if [ ${PIPESTATUS[0]} -ne 0 ]; then
	echo "couldn't find start pattern!" 1>&2
	exit 1
fi
stop=`tail -n +$start < $3 | grep -n $2 | head -n1 | cut -d: -f1; exit ${PIPESTATUS[1]}`
if [ ${PIPESTATUS[0]} -ne 0 ]; then
	echo "couldn't find end pattern!" 1>&2
	exit 1
fi

stop=$(( $stop + $start - 1))

sed "$start,$stop!d" < $3
