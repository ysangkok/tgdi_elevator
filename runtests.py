#!/usr/bin/env python3
import sys, os
import tty, termios
from colorama import Fore, Style
from subprocess import call

class Highlight:
  def __init__(self,dings,color): self.color=color; self.dings = dings
  def __enter__(self):
    print(self.color, end="")
  def __exit__(self,type,value,traceback):
    if self.dings == Fore:
      print(Fore.RESET, end="")
    else:
      assert self.dings == Style
      print(Style.RESET_ALL, end="")
    sys.stdout.flush()

def confirm():
	print("continue? y/n")
	return getch()=="y"

def getch():
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

if len(sys.argv) <= 1:
	print("Usage: postfix=\"\" ../runtests.py ../nonsynth sqrt")
	sys.exit(1)

if "postfix" not in os.environ:
	postfix = "_public"
else:
	postfix = os.environ["postfix"]

if len(sys.argv[2:]) == 0:
	li = ["display_if", "floor_request_if", "keyboard_if", "next_floor_ctrl", "sensor_if", "sqrt"] # , "cabin_ctrl"+ ["motor_if"]
else:
	li = sys.argv[2:]

fin = 0
for i in li:
	with Highlight(Style, Style.BRIGHT):
		print(i)
	sqrtifthere = "" if "motor_if" not in i else sys.argv[1] + "/mysqrt.v "
	ret = call("sh -exc 'iverilog -s " + i + "_tb " + sqrtifthere + sys.argv[1] + "/my" + i + ".v " + i + "_tb" + postfix + ".v && vvp a.out -lxt2 > " + i + "_tb_my_output.txt'", stdout=sys.stdout, stderr=sys.stdout, shell=True)
	assert ret == 0, ret
	def endi():
		for j in ["","my_"]:
			yield i + "_tb_" + j + "output.txt"
	endix = list(endi())
	ret = call("../between.sh STARTING COMPLETE {} > /tmp/{}".format(endix[0], endix[0]+"stripped"), shell=True)
	assert ret == 0, ret
	ret = call("../between.sh STARTING COMPLETE {} > /tmp/{}".format(endix[1], endix[1]+"stripped"), shell=True)
	assert ret == 0, ret
	call("rm a.out", shell=True)
	tmpstr = " ".join(["/tmp/" + x + "stripped" for x in endix])
	ret = call("diff -y " + tmpstr, shell=True)
	if ret:
		with Highlight(Fore, Fore.RED):
			print("failed!")
		if confirm():
			call("meld " + tmpstr + "&", shell=True)
		fin = 1
	else:
		with Highlight(Fore, Fore.GREEN): print("success")


sys.exit(fin)
