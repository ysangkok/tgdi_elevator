// ----------------
// Project:
// ESA Elevator
// ----------------
//
// ----------------
// Group: 062
// 1812776: Janus Troelsen
// 1816594: Zeinab Mohammadkia
// ----------------
//
// Description:
// ----------------
// iterative sqrt computation with fixed delta

`timescale 1ns / 1ns

module sqrt
#(parameter C_0=0, COUNT_BITS=0,
INITIAL_SQRT=65536,
INITIAL_N=65536
)
  (input wire                      CLK,
   input wire                      RESET,
   input wire                      NEXT,
   input wire                      PREVIOUS,
   output reg  [31:0]              SQRT); //since we use 16.16 fix point, we need [31:0]
		  

localparam  MAX_ERROR  =  32768;   // max_error = 0.5

/**************************** put your code here  ****************************/

	reg [31:0] n;
	
	reg [1:0] state;
	reg [1:0] nextstate;
	
	reg [4:0] counter;
	
	reg [31:0] H, L;
	reg [31:0] nextSQRT;
	reg [31:0] nextn;
	wire [31:0] m = (L+H)/2;
	wire [63:0] m_mal_m = (m*m) >> 16; // http://en.wikipedia.org/wiki/Q_%28number_format%29#Multiplication
	
	reg goingup;

	localparam idle = 0;
	localparam waiting = 1;
	localparam fwd = 2;
	localparam bwd = 3;

	always @ (posedge CLK) begin
		if (RESET) begin
			n <= INITIAL_N;
			SQRT <= INITIAL_SQRT;
			counter <= 0;
			state <= idle;
			goingup <= 0;
		end else begin
			if (state == idle || (NEXT && state == bwd || PREVIOUS && state == fwd) || state == waiting) begin
				goingup <= NEXT || n == INITIAL_N;
			end
			state <= nextstate;
			if (state == fwd || state == bwd) begin
				counter <= counter + 1;
				if (counter == 0) begin
					H <= goingup ? SQRT + MAX_ERROR : SQRT;
					L <= goingup ? SQRT : SQRT - MAX_ERROR;
					nextn <= goingup ? n + 65536 : n - 65536;
				end else if (counter == 15) begin
					nextSQRT <= (m_mal_m > nextn) ? (L+m)/2 : (m+H)/2;
				end else begin
					if (m_mal_m > nextn) begin
						H <= m;
					end else begin
						L <= m;
					end
				end
			end else begin
				counter <= 0;
			end
			if (state == waiting && (nextn > n && NEXT || nextn < n && PREVIOUS)) begin
				SQRT <= nextSQRT;
				n <= nextn;
			end
		end
	end

	wire [1:0] wirenext = goingup ? fwd : bwd;

	always @ (*) begin
		case (state)
			fwd:		nextstate = NEXT || PREVIOUS ? wirenext : (counter < 15) ? fwd : waiting;
			bwd:		nextstate = NEXT || PREVIOUS ? wirenext : (counter < 15) ? bwd : waiting;
			idle:		nextstate = wirenext;
			waiting:	nextstate = NEXT || PREVIOUS ? wirenext : waiting;
		endcase
	end

endmodule


module main;
  reg CLK = 0;
  reg RESET = 0;
  reg NEXT = 0;
  reg PREVIOUS = 0;
  wire [31:0] SQRT;

  sqrt mysqrt(CLK, RESET, NEXT, PREVIOUS, SQRT);

  initial 
    begin
      $dumpfile("mysqrt.vcd");
      $dumpvars(0, main);
      RESET = 1;
      #140;
      RESET = 0;
      #200;
      PREVIOUS = 0;
      $display("%b", SQRT);
      NEXT = 1;
      #2000;
      $display("%b", SQRT);
      // SQRT jetzt zugewiesen SQRT(2) = 1.41
      NEXT = 0;
      #2000;
      
      $finish ;
    end

  always
    #100 CLK = ~CLK;
endmodule
