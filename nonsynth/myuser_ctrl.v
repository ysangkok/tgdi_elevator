// ----------------
// Project:
// ESA Elevator
// ----------------
//
// ----------------
// Group: 062
// 1812776: Janus Troelsen
// 1816594: Zeinab Mohammadkia
// ----------------
//
// Description:
// ----------------
// elevator user_ctrl

`timescale 1ns / 1ns

module user_ctrl

          #(parameter FLOORS              = 5,
                      FLOOR_BITS          = 3)


          (input  wire                           CLK,
           input  wire                           RESET,

           input  wire  [(FLOOR_BITS-1)      :0] CURRENT_FLOOR_IN,           // current elevator position (floor)
           input  wire                           HALTED,                     // is set when the elevator halts at a floor
           input  wire  [(FLOORS-1)          :0] FLOOR_REQUEST,              // button inputs from keyboard interface
           input  wire                           MANUAL_DOOR_CLOSE_IN,       // manual door close from keyboard interface
           input  wire                           MANUAL_ALARM_IN,            // passenger alarm from keyboard interface

           output wire  [(FLOOR_BITS-1)      :0] CURRENT_FLOOR_OUT,          // forward current elevator position
           output wire                           MANUAL_DOOR_CLOSE_OUT,      // manual door close for cabin_ctrl
           output wire                           MANUAL_ALARM_OUT,           // passenger alarm for cabin_ctrl
           output wire  [(FLOORS-1)          :0] DESTINATIONS,               // selected destinations from
           output reg   [(FLOOR_BITS-1)      :0] CLEAR_FLOOR_BUTTON,         // disable the light for this button
           output reg                            CLEAR_FLOOR_BUTTON_VALID);  // validate reset_button

/**************************** put your code here  ****************************/

	assign CURRENT_FLOOR_OUT = RESET ? {FLOOR_BITS{1'b0}} : CURRENT_FLOOR_IN;
	assign MANUAL_DOOR_CLOSE_OUT = RESET ? 1'b0 : (HALTED & MANUAL_DOOR_CLOSE_IN);
	assign MANUAL_ALARM_OUT = RESET ? 1'b0 : MANUAL_ALARM_IN;
	assign DESTINATIONS = RESET ? {FLOORS{1'b0}} : FLOOR_REQUEST;
	always @ (*) CLEAR_FLOOR_BUTTON <= RESET ? {FLOOR_BITS{1'b0}} : CURRENT_FLOOR_IN;
	always @ (*) CLEAR_FLOOR_BUTTON_VALID <= RESET ? 1'b0 : HALTED;
	

endmodule
