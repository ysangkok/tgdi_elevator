// ----------------
// Project:
// ESA Elevator
// ----------------
//
// ----------------
// Group: 062
// 1812776: Janus Troelsen
// 1816594: Zeinab Mohammadkia
// ----------------
//
// Description:
// ----------------
// elevator cabin_ctrl

`timescale 1ns / 1ns

module cabin_ctrl

          #(parameter FLOOR_BITS             =        3, // bits required for the defined number of floors
                      CONTROL_STEP_DISTANCE  =       10, // [mm]
                      DISTANCE_BITS_BUILDING =       14, // 12.000 mm
                      DISTANCE_BITS_FLOORS   =       12, // 12 bits <> 3000mm, required for height counter
                      FLOOR_HEIGHT           =     3000, // [mm]
                      WAIT_CYCLE             = 40000000, // wait 40 Mio cycles for a delay of 2 seconds @ 20 MHz
                      WAIT_CYCLE_BITS        =       26) // requires 26 bits


          (input  wire                                 CLK,
           input  wire                                 RESET,

           input  wire                                 MANUAL_DOOR_CLOSE,     // manual door close
           input  wire                                 MANUAL_ALARM,          // passenger alarm
           input  wire                                 OBJECT_DETECTED,       // object detected
           input  wire                                 SPEED_OK    ,          // accel_ok
           input  wire                                 WEIGHT_OK,             // weight_ok
           input  wire                                 SMOKE_DETECTED,        // smoke alarm
           input  wire  [(FLOOR_BITS-1)            :0] NEXT_FLOOR,            // next floor to visit
           input  wire                                 DOOR_MOTOR_DONE,       // door movement done
           input  wire                                 ELEVATOR_MOTOR_DONE,   // cabin movement done
           input  wire                                 ELEVATOR_MOTOR_TICK,   // elevator motor drove CONTROL_STEP_DISTANCE

           output reg   [(FLOOR_BITS-1)            :0] CURRENT_FLOOR,         // cabin is at this floor
           output reg                                  HALTED,                // halted at floor
           output wire                                 OPEN_DOOR,             // open cmd
           output wire                                 CLOSE_DOOR,            // close cmd
           output wire                                 ELEVATOR_UP,           // up cmd
           output wire                                 ELEVATOR_DOWN,         // down cmd
           output reg   [(DISTANCE_BITS_BUILDING-1):0] DISTANCE,              // go distance in milimeter
           output reg                                  EMERGENCY_BRAKE);      // emergency break cmd

/**************************** put your code here  ****************************/
// FSM

localparam s_openidle     = 5'd0;
localparam s_closing      = 5'd1;
localparam s_interrupted  = 5'd2;
localparam s_opening      = 5'd3;
localparam s_closedidle   = 5'd4;
localparam s_sendopen     = 5'd5;
localparam s_sendstop     = 5'd6;
localparam s_sendclose    = 5'd7;
localparam s_goup         = 5'd8;
localparam s_godn         = 5'd9;
localparam s_driving      = 5'd10;
localparam s_retarget     = 5'd12;
localparam s_emergency    = 5'd13;

reg [4:0] state, nextstate;
reg [WAIT_CYCLE_BITS-1+1:0] waited;
reg waited2;


always @ (posedge CLK) begin
	if (state == s_opening || state == s_closing) waited <= 0;
	if (state == s_closing || state == s_openidle || state == s_interrupted || state == s_closedidle || state == s_driving) waited2 <= 0;
	if (state == s_openidle || state == s_interrupted) waited <= waited + 1;
	if (state == s_sendclose || state == s_sendstop || state == s_sendopen || state == s_goup || state == s_godn) waited2 <= waited2 + 1;
	if (RESET) begin
		state <= s_openidle;
		waited <= 0;
		waited2 <= 0;
	end else begin
		state <= nextstate;
	end
end

reg gotnew;

always @ (*) begin
	case (state)
	s_openidle     : nextstate = MANUAL_ALARM || SMOKE_DETECTED ? s_emergency : (waited >= WAIT_CYCLE || MANUAL_DOOR_CLOSE) ? s_sendclose : s_openidle;
	s_closing      : nextstate = DOOR_MOTOR_DONE ? s_closedidle : OBJECT_DETECTED ? s_sendstop : s_closing;
	s_interrupted  : nextstate = waited >= WAIT_CYCLE ? s_sendopen : s_interrupted;
	s_opening      : nextstate = DOOR_MOTOR_DONE ? s_openidle : s_opening;
	s_closedidle   : nextstate = CURRENT_FLOOR < NEXT_FLOOR ? s_goup : s_godn;
	s_sendstop     : nextstate = !OBJECT_DETECTED ? s_interrupted : s_sendstop;
	s_sendopen     : nextstate = waited2 ? s_opening : s_sendopen;
	s_sendclose    : nextstate = waited2 ? s_closing : s_sendclose;
	s_goup         : nextstate = waited2 ? s_driving : s_goup;
	s_godn         : nextstate = waited2 ? s_driving : s_godn;
	s_driving      : nextstate = gotnew ? s_retarget : ELEVATOR_MOTOR_DONE ? s_sendopen : s_driving;
	s_retarget     : nextstate = s_closedidle;
	s_emergency		: nextstate = s_emergency;
	endcase
end

assign OPEN_DOOR  =    state == s_sendstop || state == s_sendopen;
assign CLOSE_DOOR =    state == s_sendstop || state == s_sendclose;
assign ELEVATOR_UP =   state == s_goup;
assign ELEVATOR_DOWN = state == s_godn;

`define rounddown(x) (((x) <         (1*3000)) ? 3'd0 : ((x) <       (2*3000)) ? 3'd1 : ((x) <       (3*3000)) ? 3'd2 : ((x) <       (4*3000)) ? 3'd3 : 3'd4)
`define roundup(x) (((x) <=  (0*3000)) ? 3'd0 : ((x) <=   (1*3000)) ? 3'd1 : ((x) <=   (2*3000)) ? 3'd2  : ((x) <=   (3*3000)) ? 3'd3 : 3'd4)
//`define rounddown(x) ((x) / FLOOR_HEIGHT)
//`define roundup(x) (((x) + FLOOR_HEIGHT-1) / FLOOR_HEIGHT)

//reg [FLOOR_BITS-1:0] saveddestination;
reg goingup;
reg [DISTANCE_BITS_BUILDING-1:0] curr_ctrl_step;
always @ (posedge CLK) CURRENT_FLOOR <= state != s_driving || goingup ? `rounddown(curr_ctrl_step) : `roundup(curr_ctrl_step);
//assign CURRENT_FLOOR = curr_ctrl_step / FLOOR_HEIGHT;

reg didthistick;

always @ (posedge CLK) begin
	if (RESET) begin
		curr_ctrl_step <= 0;
		didthistick <= 0;
	end else begin
		if (state == s_sendopen) curr_ctrl_step <= CURRENT_FLOOR * FLOOR_HEIGHT;
		else if (state == s_retarget) curr_ctrl_step <= `rounddown(curr_ctrl_step) * 3000; // HACK TODO
		else if (ELEVATOR_MOTOR_TICK && !didthistick) begin
			curr_ctrl_step <= goingup ? (curr_ctrl_step + CONTROL_STEP_DISTANCE) : (curr_ctrl_step - CONTROL_STEP_DISTANCE) ;
			didthistick <= 1;
		end
		
		if (!ELEVATOR_MOTOR_TICK) didthistick <= 0;
	end
end

always @ (posedge CLK) begin
	if (!RESET) begin
		if (state == s_closedidle) begin
			if (goingup) begin
				DISTANCE <= (NEXT_FLOOR - CURRENT_FLOOR) * 3000;
			end else begin
				DISTANCE <= (CURRENT_FLOOR - NEXT_FLOOR) * 3000;
			end
		end

	end else begin
		DISTANCE <= 0;
	end
	
end

always @ (*) EMERGENCY_BRAKE <= RESET ? 0 : !SPEED_OK || !WEIGHT_OK;
always @ (posedge CLK) if (state != s_driving) goingup <= (NEXT_FLOOR > CURRENT_FLOOR);
always @ (*) HALTED <= state != s_driving && state != s_goup && state != s_godn && state != s_closedidle && state != s_retarget;

/*always @ (NEXT_FLOOR) begin
	if (!RESET && state == s_driving) begin
		gotnew <= 1;
	end
end*/

reg [FLOOR_BITS-1:0] prev_next_floor;

always @ (posedge CLK) begin
        if (RESET) begin
                prev_next_floor <= 0;
				gotnew <= 0;
        end else begin
			if (state == s_retarget) gotnew <= 0;
			if (state == s_driving && NEXT_FLOOR != prev_next_floor) begin
				gotnew <= 1;
			end
			if (state == s_closedidle) begin
				prev_next_floor <= NEXT_FLOOR;
			end
		end
end

// END FSM

endmodule
