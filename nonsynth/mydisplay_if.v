// ----------------
// Project:
// ESA Elevator
// ----------------
//
// ----------------
// Group: 062
// 1812776: Janus Troelsen
// 1816594: Zeinab Mohammadkia
// ----------------
//
// Description:
// ----------------
// elevator display interface

`timescale 1ns / 1ns

module display_if

          #(parameter FLOOR_BITS                =  3,
                      DISPLAY_SEGMENTS          =  7)

          (input  wire  [(FLOOR_BITS-1)      :0] CURRENT_FLOOR,    // encoded current floor input
           output wire  [(DISPLAY_SEGMENTS-1):0] ENABLE_SEGMENT);  // enable segments to show current floor

 /**************************** put your code here  ****************************/
 `define x CURRENT_FLOOR // weniger zu schreiben
 wire [DISPLAY_SEGMENTS-1:0] t;
 reg [DISPLAY_SEGMENTS-1:0] t2;
 assign t[6:0] = { // so dass es schnell kombinatorisch gemacht werden kann
     `x>=2 & `x!=7		// g
   , `x==0 | `x>=4 & `x!=7	// f
   , !(`x&1) /*gerade*/ & `x!=4	// e
   , `x!=1 & `x!=4 & `x!=7	// d
   , `x!=2			// c
   , `x!=5 & `x!=6		// b
   , `x!=1 & `x!=4		// a
 };
 always @ (*) t2 <= t; // nur weil das test ohne sequentielle logik scheitert (liest nicht nur bei steigende taktflanke)
 assign ENABLE_SEGMENT = t2;
 
endmodule
