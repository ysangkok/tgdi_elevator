// ----------------
// Project:
// ESA Elevator
// ----------------
//
// ----------------
// Group: 062
// 1812776: Janus Troelsen
// 1816594: Zeinab Mohammadkia
// ----------------
//
// Description:
// ----------------
// elevator sensor interface

`timescale 1ns / 1ns

module sensor_if

          #(parameter WEIGHT_BITS         =  10,  // weight sensor input bits
                      MAX_WEIGHT          =  500, // maximum weight [kg]
                      VELOCITY_BITS       =  11,  // velocity sensor input bits
                      VELOCITY_THRESHOLD  =  2000) // velocity threshold [mm/s]


          (input  wire                           CLK,
           input  wire                           RESET,

           input  wire                           DOOR_LIGHT_CURTAIN_IN,         // 1 if object in the way
           input  wire  [(VELOCITY_BITS-1)   :0] VELOCITY_SENSOR_IN,            // speed [cm/s]
           input  wire  [(WEIGHT_BITS-1)     :0] WEIGHT_SENSOR_IN,              // weight [kg]
           input  wire                           SMOKE_PARTICLE_SENSOR_IN,      // 1 if smoke detected

           output reg                            DOOR_LIGHT_CURTAIN_OUT,        // 0 if door is free
           output reg                            VELOCITY_SENSOR_OUT,           // 1 if speed is ok
           output reg                            WEIGHT_SENSOR_OUT,             // 1 if weight is ok
           output reg                            SMOKE_PARTICLE_SENSOR_OUT);    // 0 if smoke is ok

/**************************** put your code here  ****************************/

reg [8*4-1:0] buff;

reg [4*2-1:0] state, nextstate;
 
parameter no_problem = 2'd0;
parameter oplader_problem = 2'd1;
parameter problem = 2'd2;
 
wire [3:0] current = {
	DOOR_LIGHT_CURTAIN_IN,
	VELOCITY_SENSOR_IN > VELOCITY_THRESHOLD,
	WEIGHT_SENSOR_IN > MAX_WEIGHT,
	SMOKE_PARTICLE_SENSOR_IN
};

wire out[3:0];

genvar i;
generate
for (i=0; i<4; i=i+1) begin: perproblem

	always @ (posedge CLK)
	begin
		if (RESET) state[i*2+1:i*2] <= no_problem;
		else state[i*2+1:i*2] <= nextstate[i*2+1:i*2];
	end
	
	always @ (*)
	begin
		case (state[i*2+1:i*2])
			no_problem:		nextstate[i*2+1:i*2] = current[i] ? oplader_problem : no_problem;
			problem:		nextstate[i*2+1:i*2] = current[i] ? problem : no_problem;
			oplader_problem:	nextstate[i*2+1:i*2] = current[i] ? (buff[i*8+7:i*8] == 253 ? problem : oplader_problem) : no_problem; // 253 for iverilog, 252 for ISE
			default:		nextstate[i*2+1:i*2] = no_problem;
		endcase
	end
	
	always @ (posedge CLK)
	begin
		if (state[i*2+1:i*2] == oplader_problem) begin
			buff[i*8+7:i*8] <= buff[i*8+7:i*8] + 1;
		end
		if (state[i*2+1:i*2] == no_problem) begin
			buff[i*8+7:i*8] <= 0;
		end
	end
	
	assign out[i] = state[i*2+1:i*2] == problem && current[i];

end
endgenerate

always @ (posedge CLK) begin
	DOOR_LIGHT_CURTAIN_OUT <= 	RESET ? 0 :  out[3];
	VELOCITY_SENSOR_OUT <= 		RESET ? 1 : !out[2];
	WEIGHT_SENSOR_OUT <= 		RESET ? 1 : !out[1];
	SMOKE_PARTICLE_SENSOR_OUT <= 	RESET ? 0 :  out[0];
end

endmodule
