// ----------------
// Project:
// ESA Elevator
// ----------------
//
// ----------------
// Group: 062
// 1812776: Janus Troelsen
// 1816594: Zeinab Mohammadkia
// ----------------
//
// Description:
// ----------------
// elevator next_floor_ctrl

`timescale 1ns / 1ns

// priority encoders
`define MSB_OF(y)	(y) >= 2**4 ? 4 :	(y) >= 2**3 ? 3 :	(y) >= 2**2 ? 2 :	(y) >= 2**1 ? 1 :	(y) >= 2**0 ? 0 : 0
`define LSB_OF(y)	((y) >> 0) & 1 ? 0 :	((y) >> 1) & 1 ? 1 :	((y) >> 2) & 1 ? 2 :	((y) >> 3) & 1 ? 3 :	((y) >> 4) & 1 ? 4 : 0

module next_floor_ctrl

          #(parameter FLOORS                    = 5,
                      FLOOR_BITS                = 3)


          (input  wire                           CLK,
           input  wire                           RESET,

           input  wire                           HALTED,           // cabin halted
           input  wire  [(FLOOR_BITS-1)      :0] CURRENT_FLOOR,    // curent floor (from cabin_ctrl)

           input  wire  [(FLOORS-1)          :0] DESTINATIONS,     // destinations selected (from keyboard_if)
           input  wire  [((FLOORS*2)-1)      :0] FLOOR_REQUEST,    // elevator requests (from floor_request_if)

           output reg   [(FLOOR_BITS-1)      :0] NEXT_FLOOR);      // next floor to be visited

/**************************** put your code here  ****************************/

	reg FORWARD;
	
	wire [FLOORS-1:0] RELEVANT;

	genvar i;
	generate
		for (i=0; i<FLOORS; i=i+1) begin: perfloor
			assign RELEVANT[i] = (FLOOR_REQUEST[i*2+FORWARD] || DESTINATIONS[i]) && !(HALTED && CURRENT_FLOOR == i);
		end
	endgenerate

	wire [FLOORS-1:0] TO_VISIT = (FORWARD ?	((RELEVANT >> CURRENT_FLOOR) << CURRENT_FLOOR) : 
						((RELEVANT << (FLOORS - CURRENT_FLOOR)) >> (FLOORS - CURRENT_FLOOR)));

	wire HAS_REQUEST_SAME_DIRECTION = |TO_VISIT;

/*
	wire [FLOORS-1:0] bits_rev;
	generate
		for (i=0; i<FLOORS; i=i+1) begin
			assign bits_rev[i] = TO_VISIT[FLOORS-i-1];
		end
	endgenerate
*/

	//wire [FLOOR_BITS-1:0] NEXT_FWD = FLOORS+1 - $clog2(bits_rev+1)-1; // first least significant bit set. e.g. 'b0101 -> 0
	wire [FLOOR_BITS-1:0] NEXT_FWD = `LSB_OF(TO_VISIT); 
	//wire [FLOOR_BITS-1:0] NEXT_BWD = $clog2(TO_VISIT+1)-1; // floor(log2(x)) e.g. 'b0101 -> 2	
	wire [FLOOR_BITS-1:0] NEXT_BWD = `MSB_OF(TO_VISIT); 

	wire [FLOOR_BITS-1:0] PREPARED_NEXT_FLOOR = FORWARD ? NEXT_FWD : NEXT_BWD;
	
	wire NEXT_VALID = HAS_REQUEST_SAME_DIRECTION;

	always @ (posedge CLK) if (NEXT_VALID) NEXT_FLOOR <= PREPARED_NEXT_FLOOR;

	reg [1:0] state, nextstate;

	parameter IDLE = 2'd0;
	parameter DRIVING = 2'd1;
	parameter SWITCH_DIRECTION = 2'd2;

	always @ (posedge CLK) begin
		if (RESET) begin
			FORWARD <= 1;
		end
		if (state == SWITCH_DIRECTION) begin
			FORWARD <= !FORWARD;
		end
	end

	// FSM

	always @ (posedge CLK) begin
		if (RESET)	state <= IDLE;
		else		state <= nextstate;
	end

	always @ (*) begin
		case (state)
			IDLE:			nextstate = HAS_REQUEST_SAME_DIRECTION || !HALTED ? DRIVING : SWITCH_DIRECTION;
			DRIVING:		nextstate = HAS_REQUEST_SAME_DIRECTION || !HALTED ? DRIVING : IDLE;
			SWITCH_DIRECTION:	nextstate = IDLE;
		endcase
	end

	// END FSM
endmodule

module nfcmain();

	reg CLK;
	reg RESET;
	reg HALTED;
	reg [2:0] CURRENT_FLOOR;
	reg [4:0] DESTINATIONS;
	reg [9:0] FLOOR_REQUEST;
	wire [2:0] NEXT_FLOOR;
	
	next_floor_ctrl mynfc(CLK, RESET, HALTED, CURRENT_FLOOR, DESTINATIONS, FLOOR_REQUEST, NEXT_FLOOR);
	
	initial begin
		$dumpfile("mynext_floor_ctrl.vcd");
		$dumpvars(0, nfcmain);
		#1;
		CLK = 0;
		RESET = 1;
		CURRENT_FLOOR = 1;
		HALTED = 1;
		DESTINATIONS = 'b00000;
		FLOOR_REQUEST = 'b00_10_00_00_00;
		#2;
		RESET = 0;
		HALTED = 0;
		#2;
		CURRENT_FLOOR = 2;
		HALTED = 1;
		#2;
		HALTED = 0;
		#16;
		//if (NEXT_FLOOR !== 3) begin $display("ERROR!! %d", NEXT_FLOOR); $finish_and_return(1); end
		
		//#500000;
		$finish;
	end
	
	always #2 CLK = ~CLK;

endmodule
