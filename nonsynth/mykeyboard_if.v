// ----------------
// Project:
// ESA Elevator
// ----------------
//
// ----------------
// Group: 062
// 1812776: Janus Troelsen
// 1816594: Zeinab Mohammadkia
// ----------------
//
// Description:
// ----------------
// elevator keyboard interface

`timescale 1ns / 1ns

module keyboard_if

          #(parameter FLOORS                    = 5,
                      FLOOR_BITS                = 3)


          (input  wire                           CLK,
           input  wire                           RESET,

           input  wire  [(FLOORS-1)          :0] FLOOR_SELECT,             // connected to floor select buttons
           input  wire                           CLOSE_DOOR_IN,            // connected to passenger close door button
           input  wire                           PASSENGER_ALARM_IN,       // connected to passenger alarm button
           input  wire  [(FLOOR_BITS-1)      :0] CLEAR_FLOOR_BUTTON,       // turn of the light for this encoded floor
           input  wire                           CLEAR_FLOOR_BUTTON_VALID, // if this signal is valid

           output wire                           CLOSE_DOOR_OUT,           // skip door close delay
           output wire                           PASSENGER_ALARM_OUT,      // passenger alarm
           output wire  [(FLOORS-1)          :0] SELECTED_FLOORS,          // cabin floor selection
           output wire  [(FLOORS-1)          :0] ENLIGHT_BUTTONS);         // turn button light on

/**************************** put your code here  ****************************/
	assign PASSENGER_ALARM_OUT = RESET ? 0 : PASSENGER_ALARM_IN;
	assign CLOSE_DOOR_OUT = RESET ? 0 : CLOSE_DOOR_IN;

	reg [FLOORS-1:0] temp;
	assign ENLIGHT_BUTTONS = temp;
	assign SELECTED_FLOORS = temp;

	genvar i;
	generate
	for (i=0; i<FLOORS; i=i+1) begin: keybfloorloop
		always @ (posedge CLK) begin
			temp[i] <= RESET ? 0 : (CLEAR_FLOOR_BUTTON != i || !CLEAR_FLOOR_BUTTON_VALID) & (FLOOR_SELECT[i] | temp[i]);
		end
	end
	endgenerate

endmodule

