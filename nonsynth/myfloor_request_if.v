// ----------------
// Project:
// ESA Elevator
// ----------------
//
// ----------------
// Group: 062
// 1812776: Janus Troelsen
// 1816594: Zeinab Mohammadkia
// ----------------
//
// Description:
// ----------------
// elevator floor request interface

`timescale 1ns / 1ns

module floor_request_if

          #(parameter FLOORS                   =  5,
                      FLOOR_BITS               =  3)


          (input  wire                         CLK,
           input  wire                         RESET,

           input  wire  [(FLOOR_BITS-1):0]     CURRENT_FLOOR,
           input  wire                         HALTED,

           output wire  [((FLOORS*2)-1):0]     ENLIGHT_BUTTONS,

           input  wire  [((FLOORS*2)-1):0]     FLOOR_REQUEST_IN,     // connected to up/down buttons on the floors
           output wire  [((FLOORS*2)-1):0]     FLOOR_REQUEST_OUT);   //

/**************************** put your code here  ****************************/

	reg [((FLOORS*2)-1):0] temp;
	assign ENLIGHT_BUTTONS = temp;
	assign FLOOR_REQUEST_OUT = temp;

	genvar i;
	generate
	for (i=0; i<FLOORS; i=i+1) begin: floorreqfloorloop
		always @ (posedge CLK) begin
			temp[i*2+0] <= RESET ? 0 : (CURRENT_FLOOR != i || !HALTED) & (FLOOR_REQUEST_IN[i*2+0] | temp[i*2+0]);
			temp[i*2+1] <= RESET ? 0 : (CURRENT_FLOOR != i || !HALTED) & (FLOOR_REQUEST_IN[i*2+1] | temp[i*2+1]);
		end
	end
	endgenerate

endmodule
