// ----------------
// Project:
// ESA Elevator
// ----------------
//
// ----------------
// Group: 062
// 1812776: Janus Troelsen
// 1816594: Zeinab Mohammadkia
// ----------------
//
// Description:
// ----------------
// motor interface (door and elevator)

`timescale 1ns / 1ns

			`define MAX(p,q) ((p)>(q)?(p):(q))
			`define MIN(p,q) ((p)<(q)?(p):(q))

module motor_if

             #(parameter INITIAL_POSITION    =  0,      // inital position is at floor 0
                         MOTOR_STEP          =  50,     // [-m] (a single motor step is 50-m = 0,05mm)
                         CONTROL_STEP        =  10,     // [mm] (a control step is 10mm = 1cm)
                         DISTANCE_BITS       =  14,     // max distance 12m = 12.000 mm
                         CONTROL_STEP_BITS   =  11,     // 12m <> 1.200 control steps
                         MOTOR_STEP_BITS     =  18,     // max distance 12m = 240.000 * 50-m
                         MAX_POSITION_R      =  1200,   // [control steps] (1.200 control steps = 240.000 motor steps = 12 m)
                         MAX_POSITION_L      =  0,      // [control steps] (0 control steps = 0 motor steps = 0 m)
                         DELAY_COUNT_BITS    =  17,     // 17 bits required to count up to C_0 = 100.000
                         C_0                 =  100000, // initial counter value <> 100 Hz @ 10 MHz clock
                         C_MIN               =  250)    // minimum counter value <> 40 kHz @ 10 MHz clock

         (input  wire                       CLK,
          input  wire                       RESET,
          input  wire  [DISTANCE_BITS-1 :0] DISTANCE,       // levels [mm] / door_width [mm]
          input  wire                       ROTATE_LEFT,    // open or down
          input  wire                       ROTATE_RIGHT,   // close or up

          output reg                        CTRL_STEP_DONE, // 10mm distance is a step
          output reg                        DONE,           // motor request executed
          output wire                       A,              // motor coil a output
          output wire                       B,              // motor coil a output
          output wire                       C,              // motor coil a output
          output wire                       D);             // motor coil a output

	// define maximum and minimum position in motor_steps
	localparam MAX_POSITION	= (MAX_POSITION_R * CONTROL_STEP * 1000) / MOTOR_STEP;
	localparam MIN_POSITION	= (MAX_POSITION_L * CONTROL_STEP * 1000) / MOTOR_STEP;

	// conversion factor required to convert control steps to motor steps
	localparam CONVERSION_CTRL_TO_MOTOR_STEP	= (CONTROL_STEP	* 1000)	/ MOTOR_STEP; // 200
	localparam CONVERSION_CTRL_TO_MOTOR_STEP_BITS = (MOTOR_STEP_BITS - CONTROL_STEP_BITS + 1);

	// conversion factor to convert distance in mm to motor steps
	localparam CONVERSION_MM_TO_MOTOR_STEP	= 1000 / MOTOR_STEP; // 20
	localparam CONVERSION_MM_TO_MOTOR_STEP_BITS = (MOTOR_STEP_BITS - DISTANCE_BITS + 1);


/**************************** put your code here  ****************************/

	localparam countto = CONTROL_STEP * 1000 / MOTOR_STEP / 4;

	reg donext, doprev;
	
	parameter START = 2'd0;
	parameter ACCEL = 2'd1;
	parameter RUN	= 2'd2;
	parameter DECEL = 2'd3;
	reg [1:0] runstate, nextrunstate;

	wire [31:0] currentsqrt, nextsqrt;
	sqrt mysqrt(CLK, DONE | RESET, donext, doprev, currentsqrt);
	sqrt #(.INITIAL_N(2*65536), .INITIAL_SQRT(32'h00016a0a)) calcnext(CLK, DONE | RESET, donext, doprev, nextsqrt);

	reg [31:0] ctrlstepcounter;

	reg FORWARD;
	reg [MOTOR_STEP_BITS-1:0] S_CURRENT, S_TARGET, S_ACCEL;
	wire [MOTOR_STEP_BITS+1:0] TARGET_CAND = DISTANCE * CONVERSION_MM_TO_MOTOR_STEP;
	reg [MOTOR_STEP_BITS-1:0] LAST_POSITION;
	wire WAVE_GO = RESET ? 0 : (runstate != START && !(ROTATE_LEFT && ROTATE_RIGHT));
	wire [MOTOR_STEP_BITS-1:0] POSITION = LAST_POSITION + (!WAVE_GO ? 0 : (FORWARD ? S_CURRENT : -S_CURRENT));
	reg [DELAY_COUNT_BITS-1:0] DELAY;
	
	always @ (*) begin
		case (runstate)
			START:	nextrunstate = (S_TARGET > 0) ? ACCEL : START;
			ACCEL:	nextrunstate = (S_CURRENT >= S_TARGET/2 || DELAY == C_MIN) ? RUN : ACCEL;
			RUN:	nextrunstate = (S_TARGET - S_CURRENT == S_ACCEL) ? DECEL : RUN;
			DECEL:	nextrunstate = (S_CURRENT >= S_TARGET) ? START: DECEL;
		endcase
	end

	reg [DELAY_COUNT_BITS-1:0] counter;
	reg [3:0] pos;
	assign {A,B,C,D} = pos[3:0];
	
	always @ (posedge CLK) begin
		if (RESET) begin
			LAST_POSITION <= INITIAL_POSITION;
			DONE <= 1;
			runstate <= START;
			S_CURRENT <= 0;
			S_TARGET <= 0;
			donext <= 0;
			doprev <= 0;
		end else begin
			runstate <= nextrunstate;	

			if (nextrunstate == RUN && runstate == ACCEL) S_ACCEL <= S_CURRENT;
			
			if (ROTATE_LEFT || ROTATE_RIGHT) begin
				if (!WAVE_GO)
					S_CURRENT <= 0;			
				
				DONE <= 0;
				S_TARGET <= ((ROTATE_RIGHT && (POSITION + TARGET_CAND > MAX_POSITION))	? MAX_POSITION - POSITION	: 
								 (ROTATE_LEFT  && (POSITION - TARGET_CAND > POSITION))		? POSITION						: TARGET_CAND)
								 + (WAVE_GO ? S_CURRENT : 0);
				FORWARD <= ROTATE_RIGHT;
				ctrlstepcounter <= countto-1 - 1;
			end else begin
				if (S_TARGET <= S_CURRENT) begin
					if (S_CURRENT > 0) begin
						DONE <= 1;
						LAST_POSITION <= POSITION;
					end
					S_TARGET <= 0;
					S_CURRENT <= 0;
				end
			end
		
			if (runstate == START) begin
				S_ACCEL <= 0;
			end
////////////////////////////
		
			donext <= WAVE_GO && counter == DELAY-18 && runstate == ACCEL;
			doprev <= WAVE_GO && counter == DELAY-18 && runstate == DECEL;

			if (WAVE_GO) begin
			
				counter <= counter + 1;
				CTRL_STEP_DONE <= 0;

				if (counter == DELAY) begin	
					if (pos == 4'b1100) begin
						ctrlstepcounter <= ctrlstepcounter + 1;
						if (ctrlstepcounter >= countto - 1) begin
							CTRL_STEP_DONE <= 1;
							ctrlstepcounter <= 0;
						end
					end
							
					S_CURRENT <= S_CURRENT + 1;
					counter <= 0;
					
					pos <= (FORWARD) ? {pos[0], pos[3:1]} : {pos[2:0], pos[3]};
					
					if (runstate == ACCEL) begin
						DELAY <= `MAX((C_0 * `MAX(1, (nextsqrt - currentsqrt))) >> 16, C_MIN);
					end else if (runstate == DECEL) begin
						DELAY <= `MAX(`MIN((C_0 * `MAX(1, (nextsqrt - currentsqrt))) >> 16, C_0), C_MIN);
					end
				end
			
			end else begin
				
				CTRL_STEP_DONE <= 0;
				DELAY <= C_0;
				counter <= 0;
				pos <= 4'b1001;
				
			end
		end
	end
	
endmodule


module motormain();

	reg CLK, RESET, ROTATE_LEFT, ROTATE_RIGHT;
	reg[13:0] DISTANCE;
	wire CTRL_STEP_DONE, DONE, A, B, C, D;

	motor_if mymotor
	 (CLK,
		RESET,
		DISTANCE,	// levels [mm] / door_width [mm]
		ROTATE_LEFT,	// open or down
		ROTATE_RIGHT,	// close or up

		CTRL_STEP_DONE, // 10mm distance is a step
		DONE,		// motor request executed
		A,		// motor coil a output
		B,		// motor coil a output
		C,		// motor coil a output
		D); 
	initial begin
		$dumpfile("mymotor_if.vcd");
		$dumpvars(0, motormain);
		ROTATE_LEFT = 0;
		ROTATE_RIGHT = 0;
		CLK = 0;
		RESET = 1;
		#1;
		#140;
		DISTANCE = 10;
		ROTATE_RIGHT = 1;
		RESET = 0;
		#2;
		DISTANCE = 0;
		ROTATE_RIGHT = 0;
		#90000;
		DISTANCE = 4;
		ROTATE_RIGHT = 1;
		#2;
		ROTATE_RIGHT = 0;
		DISTANCE = 0;
		#300000;
		$finish;
	end
	
	always #1 CLK = ~CLK;
endmodule
