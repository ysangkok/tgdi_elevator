Chronologic VCS simulator copyright 1991-2011
Contains Synopsys proprietary information.
Compiler version E-2011.03-SP1_Full64; Runtime version E-2011.03-SP1_Full64;  Jan 23 20:38 2013
************ STARTING SIMULATION ************
motor right rotate 1000 mm
                1983 ms: motor stopped correctly @       1000 mm
received  100 control steps of          10  mm
this took                 1983 ms
motor right rotate 2000 mm
                4964 ms: motor stopped correctly @       3000 mm
received  200 control steps of          10  mm
this took                 2981 ms
motor left rotate 3000 mm
changing direction -> going down
                8943 ms: motor stopped correctly @          0 mm
received  300 control steps of          10  mm
this took                 3978 ms
motor right rotate 6000 mm
changing direction -> going up
               15922 ms: motor stopped correctly @       6000 mm
received  600 control steps of          10  mm
this took                 6979 ms
motor right rotate 3000 mm
               19892 ms: motor stopped correctly @       9000 mm
received  300 control steps of          10  mm
this took                 3969 ms
motor right rotate 6000 mm
               23858 ms: motor stopped correctly @ max. position of        1200 mm
last run was only       3000 mm, max. position reached
this took                 3965 ms
motor left rotate 9000 mm
changing direction -> going down
               33832 ms: motor stopped correctly @       3000 mm
received  900 control steps of          10  mm
this took                 9974 ms
motor left rotate 6000 mm
               37791 ms: motor stopped correctly @           0 cm
last run was only       3000 mm, min. position reached
this took                 3958 ms
motor right rotate 9000 mm
changing direction -> going up
updating motor target: right rotate 3000 mm from current position       3000 mm
               44753 ms: motor stopped correctly @       6000 mm
received  300 control steps of          10  mm
this took                 3472 ms
************ SIMULATION COMPLETE ************
$finish called from file "motor_if_tb.v", line 490.
$finish at simulation time       44753062850000
           V C S   S i m u l a t i o n   R e p o r t 
Time: 44753062850000 ps
CPU Time:    167.360 seconds;       Data structure size:   0.0Mb
Wed Jan 23 20:41:00 2013
