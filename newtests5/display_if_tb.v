// ----------------
// Project:
// ESA Elevator
// ----------------
//
// Description:
// ----------------
// display_if.v testbench
//
// Version History:
// ----------------
// 120208: BT: initial version

`timescale 1ns / 1ps

module display_if_tb;

  //****************** SIMULATION PARAMETERS *******************//  
  localparam CLK_PERIOD       =  50; // [ns] -> 20 MHz
  localparam NUM_FLOORS       =  10; // number of floors 
  localparam DISPLAY_SEGMENTS =   7;
  //***********************************************************//
  

  //*************** DERIVED SIMULATION PARAMETERS *************// 
  localparam FLOOR_BITS            = $clog2(NUM_FLOORS);
  //***********************************************************//
  

  //********************* MODULE INPUTS ***********************// 
  reg                            CLK;
  reg                            RESET;
  reg [(FLOOR_BITS-1)        :0] CURRENT_FLOOR;
  reg [(FLOOR_BITS-1)        :0] binOut;
  reg [7                     :0] asciOut;
  //***********************************************************//  


  //********************* MODULE OUTPUTS **********************//
  wire  [(DISPLAY_SEGMENTS-1):0] ENABLE_SEGMENT; 
  //***********************************************************// 
  

  //******************* UUT INSTANTIATION *********************// 
  display_if #(
               .DISPLAY_SEGMENTS (DISPLAY_SEGMENTS),
               .FLOOR_BITS       (FLOOR_BITS))
               
      DISPLAY_IF_i (
                    .CURRENT_FLOOR(CURRENT_FLOOR),
                    .ENABLE_SEGMENT(ENABLE_SEGMENT));                                
  //***********************************************************//
  

  //******************* 20 MHz CLOCK SIGNAL *******************//   
  always begin 
    #(CLK_PERIOD/2) CLK = ~CLK;
  end
  //***********************************************************// 


  //********************* TEST INITIATION *********************//  
  initial begin
    // Initialize Inputs
    CLK                      = 0;
    RESET                    = 1;
    CURRENT_FLOOR            = 0;

    // Wait 125 ns for global reset to finish
    #(3*CLK_PERIOD/2);
    RESET = 0;
    $display("************ STARTING SIMULATION ************");  
    #(5*CLK_PERIOD);
    //--------------------------
    // SELECT FLOORS 
    //--------------------------
    CURRENT_FLOOR    = 0;
    #(CLK_PERIOD);
    CURRENT_FLOOR    = 1;
    #(CLK_PERIOD);
    CURRENT_FLOOR    = 2;
    #(CLK_PERIOD);
    CURRENT_FLOOR    = 3;
    #(CLK_PERIOD);
    CURRENT_FLOOR    = 4;
    #(CLK_PERIOD);
    CURRENT_FLOOR    = 5;
    #(CLK_PERIOD);
    CURRENT_FLOOR    = 6;
    #(CLK_PERIOD);
    CURRENT_FLOOR    = 7;
    #(CLK_PERIOD);
    CURRENT_FLOOR    = 8;
    #(CLK_PERIOD);
    CURRENT_FLOOR    = 9;   
    #(5*CLK_PERIOD);  
    $display("************ SIMULATION COMPLETE ************");
    $finish;
  end
  //***********************************************************// 


  //****************** VISUALISATION PROCESS ******************//
  always @(ENABLE_SEGMENT)
    case (ENABLE_SEGMENT)
      7'b0111111 : begin
                   binOut  = 4'h0;
                   asciOut = 8'h30;
                   end
      7'b0000110 : begin
                   binOut  = 4'h1;
                   asciOut = 8'h31;
                   end
      7'b1011011 : begin
                   binOut  = 4'h2;
                   asciOut = 8'h32;
                   end
      7'b1001111 : begin
                   binOut  = 4'h3;
                   asciOut = 8'h33;
                   end
      7'b1100110 : begin
                   binOut  = 4'h4;
                   asciOut = 8'h34;
                   end
      7'b1101101 : begin
                   binOut  = 4'h5;
                   asciOut = 8'h35;
                   end
      7'b1111101 : begin
                   binOut  = 4'h6;
                   asciOut = 8'h36;
                   end
      7'b0000111 : begin
                   binOut  = 4'h7;
                   asciOut = 8'h37;
                   end
      7'b1111111 : begin
                   binOut  = 4'h8;
                   asciOut = 8'h38;
                   end
      7'b1101111 : begin
                   binOut  = 4'h9;
                   asciOut = 8'h39;
                   end
      default:     begin
                   binOut  = 4'h0;
                   asciOut = 8'h30;
                   end
    endcase


  always@(asciOut) begin
    $display("Current floor: %c", asciOut);  
  end  
  //***********************************************************// 


  //******************* PARAMETER FUNCTIONS *******************//
  //ceil of the log base 2
  function integer ceil_log2;
    input [31:0] value;
    for (ceil_log2=0; value>0; ceil_log2=ceil_log2+1)
      value = value>>1;
  endfunction
  
  // value cannot be less than 1
  function integer min_1;
    input [31:0] value;
    if (value == 0)
      min_1 = 1;
    else
      min_1 = value;
  endfunction 
  //***********************************************************//      
  
    
endmodule

