// ----------------
// Project:
// ESA Elevator
// ----------------
//
// Description:
// ----------------
// sqrt.v testbench
//
// Version History:
// ----------------
// 120217: BT: initial version

`timescale 1ns / 1ns

module sqrt_tb;

  //****************** SIMULATION PARAMETERS *******************//  
  localparam CLK_PERIOD       =      50; // [ns] -> 20 MHz
  localparam C_0              =  100000; // 100000
  //***********************************************************//


  //*************** DERIVED SIMULATION PARAMETERS *************// 
  localparam COUNT_BITS       = $clog2(C_0);
  //***********************************************************//


  //********************* MODULE INPUTS ***********************// 
  reg CLK;
  reg RESET;
  reg NEXT;
  reg PREVIOUS;
  //***********************************************************// 


  //********************* MODULE OUTPUTS **********************//
  //wire  [COUNT_BITS-1:0]  C_N;       
  wire  [31:0]  SQRT;       
  //***********************************************************//  


  //******************* UUT INSTANTIATION *********************// 
  sqrt #()
       uut (
            .CLK(CLK), 
            .RESET(RESET), 
            .NEXT(NEXT), 
            .PREVIOUS(PREVIOUS), 
            .SQRT(SQRT));
  //******************* UUT INSTANTIATION *********************// 
     

  //******************* SIMULATION VARIABLES ******************// 
  reg [31:0] STEP;
  reg [31:0] TOTAL_STEPS;
  integer    i;
  reg        NXT;
  reg        PRV;
  //***********************************************************// 
  
          
  //********************* TEST INITIATION *********************//  
  initial begin
		$dumpfile("sqrt_tb.lxt2");
		$dumpvars(0, sqrt_tb);
                STEP        = 0;
                TOTAL_STEPS = 32;
                NXT         =  0;
                PRV         =  0;
		// Initialize Inputs
		CLK      = 0;
                NEXT     = 0;
                PREVIOUS = 0;
                RESET    = 1;
                #(3*CLK_PERIOD/2);
                RESET    = 0;
                $display("************ STARTING SIMULATION ************"); 
                #(CLK_PERIOD);
                for(i=0;i<TOTAL_STEPS/2;i=i+1) begin        
                request_NEXT;
                #(16*CLK_PERIOD);  
                end
                for(i=0;i<TOTAL_STEPS/2;i=i+1) begin        
                request_PREVIOUS;
                #(16*CLK_PERIOD);  
                end
                #(100*CLK_PERIOD);
                for(i=0;i<TOTAL_STEPS/2;i=i+1) begin        
                request_NEXT;
                #(16*CLK_PERIOD);  
                end
                for(i=0;i<TOTAL_STEPS/2;i=i+1) begin        
                request_PREVIOUS;
                #(16*CLK_PERIOD);  
                end
                $display("************ SIMULATION COMPLETE ************");
                $finish;
  end       
  //***********************************************************// 
  initial begin
  #(100000*CLK_PERIOD)
  $display("************ SIMULATION KILLED BECAUSE OF ERROR ************");
  $finish;
end


  //******************* 20 MHz CLOCK SIGNAL *******************// 
  always begin
    #(CLK_PERIOD/2) CLK = ~CLK;
  end
  //***********************************************************// 
 
  

  //****************** VISUALISATION PROCESS ******************//
	 integer iCnt;
	 integer res;
	 integer wertigkeit;
  reg [32:0] nachkommastellen;
  integer ersteGesetzteNachkommastelle;
  always@(posedge NEXT or posedge PREVIOUS) begin
      #100;
    $write("Step: %d: current counter value: %d, %b", STEP, SQRT, SQRT);
	 $write(" Decimal number: ~ ");
	 res = 0;
	 for (iCnt = 0; iCnt <= 16; iCnt = iCnt + 1) begin
		wertigkeit = (1 << (16 - iCnt));
		if (SQRT[32 - iCnt] == 1)
			res = res + wertigkeit;
	 end
	 $write("%0d.", res);
	 nachkommastellen = 0;
	 ersteGesetzteNachkommastelle = -1;
	 if (SQRT[0] == 1)
	 begin
		ersteGesetzteNachkommastelle = 4;
		nachkommastellen = nachkommastellen + 000015259; //inexact...
	 end
	 if (SQRT[1] == 1)
	 begin
		ersteGesetzteNachkommastelle = 4;
		nachkommastellen = nachkommastellen + 000030518; //inexact...
	 end
	 if (SQRT[2] == 1)
	 begin
		ersteGesetzteNachkommastelle = 4;
		nachkommastellen = nachkommastellen + 000061035; //inexact...
	 end
	 if (SQRT[3] == 1)
	 begin
		ersteGesetzteNachkommastelle = 3;
		nachkommastellen = nachkommastellen + 000122070; //inexact...
	 end
	 if (SQRT[4] == 1)
	 begin
		ersteGesetzteNachkommastelle = 3;
		nachkommastellen = nachkommastellen + 000244141; //inexact...
	 end
	 if (SQRT[5] == 1)
	 begin
		ersteGesetzteNachkommastelle = 3;
		nachkommastellen = nachkommastellen + 000488281; //inexact...
	 end
	 if (SQRT[6] == 1)
	 begin
		ersteGesetzteNachkommastelle = 3;
		nachkommastellen = nachkommastellen + 000976563; //inexact...
	 end
	 if (SQRT[7] == 1)
	 begin
		ersteGesetzteNachkommastelle = 2;
		nachkommastellen = nachkommastellen + 001953125;
	 end
	 if (SQRT[8] == 1)
	 begin
		ersteGesetzteNachkommastelle = 2;
		nachkommastellen = nachkommastellen + 003906250;
	 end
	 if (SQRT[9] == 1)
	 begin
		ersteGesetzteNachkommastelle = 2;
		nachkommastellen = nachkommastellen + 007812500;
	 end
	 if (SQRT[10] == 1)
	 begin
		ersteGesetzteNachkommastelle = 1;
		nachkommastellen = nachkommastellen + 015625000;
	 end
	 if (SQRT[11] == 1)
	 begin
		ersteGesetzteNachkommastelle = 1;
		nachkommastellen = nachkommastellen + 031250000;
	 end
	 if (SQRT[12] == 1)
	 begin
		ersteGesetzteNachkommastelle = 1;
		nachkommastellen = nachkommastellen + 062500000;
	 end
	 if (SQRT[13] == 1)
	 begin
		ersteGesetzteNachkommastelle = 0;
		nachkommastellen = nachkommastellen + 125000000;
	 end
	 if (SQRT[14] == 1)
	 begin
		ersteGesetzteNachkommastelle = 0;
		nachkommastellen = nachkommastellen + 250000000;
	 end
 	 if (SQRT[15] == 1)
	 begin
		ersteGesetzteNachkommastelle = 0;
		nachkommastellen = nachkommastellen + 500000000;
	 end
	 
	 if (ersteGesetzteNachkommastelle != 42) begin
		 for (iCnt = ersteGesetzteNachkommastelle; iCnt > 0; iCnt = iCnt - 1)
			$write("0");
	 end
	 $display("%0d", nachkommastellen);
  end    
  //***********************************************************// 
  

  //******************** SIMULATION TASKS  ********************//  
  task request_NEXT;
     begin
         NXT  = 1;
         NEXT = 0;
         #50;
         NEXT = 1;
         if (PRV) begin
           PRV  = 0;
           STEP = STEP;
         end
         else begin
           STEP = STEP + 1;
         end
         #50;
	 NEXT = 0;
    end
  endtask  
  
  task request_PREVIOUS;
     begin
         PRV  = 1;
         PREVIOUS  = 0;
         #50;
         PREVIOUS  = 1;
         
         if(NXT || STEP == 1) begin
           NXT = 0;
           STEP = STEP;
         end         
         else begin
           STEP = STEP - 1;
         end
         #50;
	 PREVIOUS  = 0;
    end
  endtask 
  //***********************************************************// 


  //******************* PARAMETER FUNCTIONS *******************//
  //ceil of the log base 2
  function integer ceil_log2;
    input [31:0] value;
    for (ceil_log2=0; value>0; ceil_log2=ceil_log2+1)
      value = value>>1;
  endfunction
  
  // value cannot be less than 1
  function integer min_1;
    input [31:0] value;
    if (value == 0)
      min_1 = 1;
    else
      min_1 = value;
  endfunction 
  //***********************************************************//    

      
endmodule
